/* eslint-disable react/prop-types */
import { TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Paper } from '@mui/material';

const DataTable = ({ sensorData }) => {
  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Tower ID</TableCell>
            <TableCell>Latitude</TableCell>
            <TableCell>Longitude</TableCell>
            <TableCell>Temperature</TableCell>
            <TableCell>Power Source</TableCell>
            <TableCell>Fuel Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {sensorData.map((data, index) => (
            <TableRow key={index}>
              <TableCell>{data.towerId}</TableCell>
              <TableCell>{data.location.latitude}</TableCell>
              <TableCell>{data.location.longitude}</TableCell>
              <TableCell>{data.temperature}</TableCell>
              <TableCell>{data.powerSource}</TableCell>
              <TableCell>{data.fuelStatus}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default DataTable;
