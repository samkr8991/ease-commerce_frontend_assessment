import { useState, useEffect } from 'react';
import { Grid, Paper } from '@mui/material';
import GeoMap from './GeoMap';
import DataTable from './DataTable';
import WebSocketService from '../services/webSocketService';
import ApiService from '../services/apiService';

const LandingPage = () => {
  const [sensorData, setSensorData] = useState([]);
  const [alarms, setAlarms] = useState([]);

  useEffect(() => {
    // Connect to WebSocket server for real-time updates
    const socket = WebSocketService.connect();

    // Subscribe to real-time updates
    socket.on('sensorData', (newSensorData) => {
      setSensorData(newSensorData);
    });

    socket.on('alarms', (newAlarms) => {
      setAlarms(newAlarms);
    });

    // Fetch initial sensor data
    ApiService.fetchSensorData()
      .then((data) => setSensorData(data))
      .catch((error) => console.error('Error fetching sensor data:', error));

    // Clean up WebSocket connection
    return () => {
      socket.disconnect();
    };
  }, []);

  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <Paper elevation={3}>
          <GeoMap alarms={alarms} />
        </Paper>
      </Grid>
      <Grid item xs={12}>
        <Paper elevation={3}>
          <DataTable sensorData={sensorData} />
        </Paper>
      </Grid>
    </Grid>
  );
};

export default LandingPage;
