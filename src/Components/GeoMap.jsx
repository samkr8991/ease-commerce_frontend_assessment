/* eslint-disable react/prop-types */
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';

const GeoMap = ({ alarms }) => {
  return (
    <MapContainer center={[0, 0]} zoom={2} style={{ height: '400px', width: '100%' }}>
      <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
      {alarms.map((alarm, index) => (
        <Marker key={index} position={[alarm.latitude, alarm.longitude]}>
          <Popup>
            <div>
              <h3>Alarm</h3>
              <p>Tower ID: {alarm.towerId}</p>
              <p>Location: {alarm.latitude}, {alarm.longitude}</p>
              {/* Add more alarm details as needed */}
            </div>
          </Popup>
        </Marker>
      ))}
    </MapContainer>
  );
};

export default GeoMap;
