const ApiService = {
  fetchSensorData: async () => {
    const response = await fetch("http://localhost:4000/api/v1/sensor-data");
    if (!response.ok) {
      throw new Error("Failed to fetch sensor data");
    }
    return response.json();
  },
};

export default ApiService;
