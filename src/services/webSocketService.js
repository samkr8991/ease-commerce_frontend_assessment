import io from 'socket.io-client';

const WebSocketService = {
  connect: () => {
    const socket = io('http://localhost:8000'); // Replace with your WebSocket server URL
    return socket;
  },
};

export default WebSocketService;
